package com.spring.cloud.resourceserver.client.converter

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import assertk.assertions.isEmpty
import assertk.assertions.isNotEmpty
import org.junit.jupiter.api.Test
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.jwt.JwtClaimNames.SUB

@ExperimentalStdlibApi
class ResourceAccessJwtGrantedAuthorityConverterTest {

    companion object {
        private const val APPLICATION_NAME = "service-a"
        private const val JWT_ACCESS_TOKEN =
            "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJKeUVlaGdoenRIZk5wQzhHRG9oY1V0bGlTREJhOUtlaHRPalI2eDRGZmxFIn0.eyJleHAiOjE2MTUwNjk4NzMsImlzcyI6Imh0dHBzOi8vMTAuMC4wLjE6ODAvYXV0aC9yZWFsbXMvZGV2Iiwic3ViIjoiYjIyODc2ZjUtOTdiYS00MDRhLWJjMTctNjQwMjk0N2NkZGFiIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoidGVzdC1jbGllbnQiLCJzZXNzaW9uX3N0YXRlIjoiZjA0ZTA3YzUtZmQ2My00YmZjLWFlMGItZWM5MDU5MmU2ZDY1In0.PPV5b-RQyqzdZvjKVHPkgPBIwLBqS-09YMg-wKZA1Lzo1givqboEE95U8qNLR9AM69wJsIHUem7vul6nBwPWRO5Cb7_Jo68G0gGs2XrlhzgTFVkbcYlJhkdW_IGRxGDTlM7dhUVezRAY7skTcX6EHVw9EM1FMCkpqwHUb-JHUb2ftUhoVhUzg7bWzkaUE_Uj_a9AlPFg8Paw1L1SL2lg7BD6gr51EWSdkhLafCt0z_PY8-Cs4wWRfc52m72TIZgwwLeSnKsvY6hDr5uhN0RF3k65ylHp-Io0UUhTTC3FHGLXAiHCz8AxBXQmoT7KknBPnupqoMG0Aa8MnmdGSAiuKQ"
        private const val RESOURCE_ACCESS_CLAIM = "resource_access"
        private const val ROLES_CLAIM = "roles"
    }

    private val underTest = ResourceAccessJwtGrantedAuthorityConverter(APPLICATION_NAME)

    @Test
    fun `given JWT has no resource roles for application when JWT is converted to set of granted authorities then empty set is returned`() {
        val jwt = Jwt(JWT_ACCESS_TOKEN, null, null, buildHeaders(), buildClaims(false))

        val grantedAuthorities = underTest.convert(jwt)

        assertThat(grantedAuthorities).isEmpty()
    }

    @Test
    fun `given JWT has resource roles for application when JWT is converted to set of granted authorities then only roles for current application are returned`() {
        val jwt = Jwt(JWT_ACCESS_TOKEN, null, null, buildHeaders(), buildClaims(true))

        val grantedAuthorities = underTest.convert(jwt)

        assertThat(grantedAuthorities).isNotEmpty()
        assertThat(grantedAuthorities.map(GrantedAuthority::getAuthority)).containsExactlyInAnyOrder("ROLE_1", "ROLE_2")
    }

    private fun buildHeaders(): Map<String, Any> {
        return buildMap {
            put(SUB, "a885feb9-63a5-4d60-9466-cfc7d346d173")
        }
    }

    private fun buildClaims(isForCurrentResource: Boolean): Map<String, Any> {
        return buildMap {
            put(SUB, "a885feb9-63a5-4d60-9466-cfc7d346d173")
            put(
                RESOURCE_ACCESS_CLAIM,
                buildMap<String, Any> {
                    if (isForCurrentResource) {
                        put(
                            APPLICATION_NAME,
                            buildMap<String, Any> {
                                put(
                                    ROLES_CLAIM,
                                    buildList {
                                        add("ROLE_1")
                                        add("ROLE_2")
                                    }
                                )
                            }
                        )
                    }

                    put(
                        "service-b",
                        buildMap<String, Any> {
                            put(
                                ROLES_CLAIM,
                                buildList {
                                    add("ROLE_3")
                                }
                            )
                        }
                    )

                    put(
                        "service-c",
                        buildMap<String, Any> {
                            put(
                                ROLES_CLAIM,
                                buildList {
                                    add("ROLE_4")
                                    add("ROLE_5")
                                    add("ROLE_6")
                                }
                            )
                        }
                    )
                }
            )
        }
    }
}
