package com.spring.cloud.resourceserver.client.config

import com.spring.cloud.resourceserver.client.converter.ResourceAccessJwtGrantedAuthorityConverter
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.core.annotation.Order
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.web.servlet.invoke
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter

@EnableWebSecurity
@Order(1)
@EnableGlobalMethodSecurity(prePostEnabled = true)
class ServletOauth2ResourceServerConfiguration : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) = http {
        oauth2ResourceServer {
            jwt {
            }
        }
    }

    @Bean
    @ConditionalOnMissingBean
    fun jwtAuthenticationConverter(jwtGrantedAuthorityConverter: ResourceAccessJwtGrantedAuthorityConverter): JwtAuthenticationConverter =
        JwtAuthenticationConverter().apply {
            setJwtGrantedAuthoritiesConverter(jwtGrantedAuthorityConverter)
        }
}
