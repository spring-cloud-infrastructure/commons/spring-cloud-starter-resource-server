package com.spring.cloud.resourceserver.client.authority.service.a

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited
import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.ANNOTATION_CLASS
import kotlin.annotation.AnnotationTarget.CLASS
import kotlin.annotation.AnnotationTarget.FUNCTION
import kotlin.annotation.AnnotationTarget.PROPERTY_GETTER
import kotlin.annotation.AnnotationTarget.PROPERTY_SETTER

@Target(
    FUNCTION,
    PROPERTY_GETTER,
    PROPERTY_SETTER,
    ANNOTATION_CLASS,
    CLASS
)
@Retention(RUNTIME)
@MustBeDocumented
@Inherited
@PreAuthorize("hasAuthority('SCHEDULED_JOB')")
annotation class HasScheduledJobAuthority
