package com.spring.cloud.resourceserver.client.converter

import mu.KotlinLogging.logger
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.convert.converter.Converter
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Component

@Component
class ResourceAccessJwtGrantedAuthorityConverter(
    @Value("\${spring.application.name}") private val applicationName: String,
) : Converter<Jwt, Collection<GrantedAuthority>> {

    private val logger = logger { }

    companion object {
        private const val RESOURCE_ACCESS_CLAIM = "resource_access"
        private const val ROLES_CLAIM = "roles"
    }

    override fun convert(token: Jwt): Collection<GrantedAuthority> {
        val grantedAuthorities = token.takeIf(::containsResourceAccessClaim)
            ?.let(::getResourceAccessClaim)
            ?.takeIf(::containsRolesWrapperForCurrentApplication)
            ?.let(::getRolesWrapperForCurrentApplication)
            ?.takeIf(::containsRolesClaim)
            ?.let(::getRolesClaim)
            ?.asSequence()
            ?.filter(String::isNotBlank)
            ?.map(StringUtils::upperCase)
            ?.map(::SimpleGrantedAuthority)
            ?.distinct()
            ?.toSet()
            ?: emptySet()

        if (grantedAuthorities.isEmpty()) {
            logger.warn { "No granted authorities found for current resource: [$applicationName] in JWT: [${token.subject}]" }
        }

        return grantedAuthorities
    }

    private fun containsResourceAccessClaim(token: Jwt): Boolean = token.containsClaim(RESOURCE_ACCESS_CLAIM)

    private fun getResourceAccessClaim(token: Jwt): Map<String, Any> = token.getClaimAsMap(RESOURCE_ACCESS_CLAIM)

    private fun containsRolesWrapperForCurrentApplication(resourceAccess: Map<String, Any>): Boolean =
        resourceAccess.containsKey(applicationName)

    private fun getRolesWrapperForCurrentApplication(resourceAccess: Map<String, Any>): Map<String, Any> =
        resourceAccess[applicationName] as Map<String, Any>

    private fun containsRolesClaim(rolesWrapper: Map<String, Any>): Boolean = rolesWrapper.containsKey(ROLES_CLAIM)

    private fun getRolesClaim(rolesWrapper: Map<String, Any>): List<String> = rolesWrapper[ROLES_CLAIM] as List<String>
}
