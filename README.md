# Spring Cloud Starter Resource Server

Provides Spring auto-configuration for resource server.

## 1. Install to local Maven repository

```shell
./gradlew clean publishToMavenLocal
```

## 2. Use in a project

- Add dependency:

```kotlin
implementation("com.spring.cloud:spring-cloud-starter-resource-server:<VERSION>")
```

- Define JWT properties:

```yaml
spring:
  security:
    oauth2:
      resourceserver:
        jwt:
          issuer-uri: www.auth-server.com/auth/realms/master
          jwk-set-uri: www.auth-server.com/auth/realms/master/protocol/openid-connect/certs
```
