import com.diffplug.gradle.spotless.SpotlessExtension
import com.diffplug.spotless.LineEnding.UNIX
import org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED
import org.gradle.api.tasks.testing.logging.TestLogEvent.PASSED
import org.gradle.api.tasks.testing.logging.TestLogEvent.SKIPPED
import org.jetbrains.kotlin.gradle.plugin.statistics.ReportStatisticsToElasticSearch.url
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar

plugins {
    val springBootPluginVersion = "2.5.5"
    val springDependencyManagementPluginVersion = "1.0.11.RELEASE"
    val gitPropertiesPluginVersion = "2.3.1"
    val spotlessPluginVersion = "5.16.0"
    val kotlinPluginVersion = "1.5.31"
    `java-library`
    `maven-publish`
    id("org.springframework.boot") version springBootPluginVersion
    id("io.spring.dependency-management") version springDependencyManagementPluginVersion
    id("com.gorylenko.gradle-git-properties") version gitPropertiesPluginVersion
    id("com.diffplug.spotless") version spotlessPluginVersion
    kotlin("jvm") version kotlinPluginVersion
    kotlin("plugin.spring") version kotlinPluginVersion
}

group = "com.spring.cloud"
version = "1.0.0-SNAPSHOT"
val javaVersion = JavaVersion.VERSION_15

repositories {
    mavenLocal()
    mavenCentral()
    maven {
        name = "GitLab"
        url = uri("https://gitlab.com/api/v4/groups/9994989/-/packages/maven")
    }
}

dependencies {
    val kotlinLoggingJvmVersion = "2.0.11"
    api(kotlin("stdlib-jdk8"))
    api(kotlin("reflect"))
    api("org.springframework.boot:spring-boot-starter-web")
    api("org.springframework.boot:spring-boot-starter-security")
    api("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    api("org.apache.commons:commons-lang3")
    api("io.github.microutils:kotlin-logging-jvm:$kotlinLoggingJvmVersion")

    val springMockkVersion = "3.0.1"
    val assertkVersion = "0.24"
    testImplementation("com.ninja-squad:springmockk:$springMockkVersion")
    testImplementation("com.willowtreeapps.assertk:assertk:$assertkVersion")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(module = "mockito-core")
        exclude(module = "mockito-junit-jupiter")
    }
}

dependencyManagement {
    val springCloudDependenciesBomVersion = "2020.0.4"
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:$springCloudDependenciesBomVersion")
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = javaVersion.toString()
    }
}

tasks.getByName<BootJar>("bootJar") {
    enabled = false
}

tasks.getByName<Jar>("jar") {
    enabled = true
}

publishing {
    publications {
        create<MavenPublication>("project") {
            from(components["kotlin"])
        }
    }

    repositories {
        maven {
            name = "GitLab"
            url = uri("https://gitlab.com/api/v4/projects/22388503/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        showExceptions = true
        showStandardStreams = true
        events(PASSED, SKIPPED, FAILED)
    }
}

configure<SpotlessExtension> {
    encoding("UTF-8")
    lineEndings = UNIX
    val ktlintVersion = "0.42.1"
    kotlin {
        targetExclude("build/generated/**/*")
        trimTrailingWhitespace()
        endWithNewline()
        indentWithSpaces()
        ktlint(ktlintVersion).userData(
            mapOf(
                // list of all standard ktlint rules: https://github.com/pinterest/ktlint#standard-rules
                "charset" to "UTF-8",
                "end_of_line" to "lf",
                "indent_style" to "space",
                "indent_size" to "4",
                "ij_continuation_indent_size" to "4",
                "no-semi" to "true",
                "no-unused-imports" to "true",
                "no-consecutive-blank-lines" to "true",
                "no-blank-line-before-rbrace" to "true",
                "no-trailing-spaces" to "true",
                "no-unit-return" to "true",
                "no-empty-class-body" to "true",
                "no-wildcard-imports" to "true",
                "chain-wrapping" to "true",
                "no-line-break-before-assignment" to "true",
                "string-template" to "true",
                "modifier-order" to "true",
                "colon-spacing" to "true",
                "comma-spacing" to "true",
                "curly-spacing" to "true",
                "dot-spacing" to "true",
                "double-colon-spacing" to "true",
                "keyword-spacing" to "true",
                "op-spacing" to "true",
                "paren-spacing" to "true",
                "range-spacing" to "true",
                "insert_final_newline" to "true",
                "final-newline" to "true",
                "import-ordering" to "true",
                "ij_kotlin_imports_layout" to "*,java.**,javax.**,kotlin.**,^"
            )
        )
    }

    kotlinGradle {
        trimTrailingWhitespace()
        endWithNewline()
        indentWithSpaces()
        ktlint(ktlintVersion)
    }
}
